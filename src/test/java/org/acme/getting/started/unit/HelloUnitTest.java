package org.acme.getting.started.unit;

import org.acme.getting.started.GreetingResource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HelloUnitTest {

    @Test
    public void helloUnitTest() {
        GreetingResource greet = new GreetingResource();
        Assertions.assertEquals("Hola mundo", greet.hello());
    }
}
